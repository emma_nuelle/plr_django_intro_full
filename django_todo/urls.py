from django.conf.urls import include, url
from django.contrib import admin
from todo.views import ToDoListView, ActiveToDoListView, DoneToDoListView, ToggleDoneView, DeleteToDoView, UpdateToDoView
from django.conf import settings

urlpatterns = [
    # Examples:
    # url(r'^$', 'django_todo.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', ToDoListView.as_view(), name='home'),
    url(r'^active/$', ActiveToDoListView.as_view(), name='active'),
    url(r'^done/$', DoneToDoListView.as_view(), name='done'),
    url(r'^toggle/(?P<pk>\d+)/$', ToggleDoneView.as_view(), name='toggle'),
    url(r'^delete/(?P<pk>\d+)/$', DeleteToDoView.as_view(), name='delete'),
    url(r'^update/(?P<pk>\d+)/$', UpdateToDoView.as_view(), name='update'),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]