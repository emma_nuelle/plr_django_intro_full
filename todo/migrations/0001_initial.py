# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ToDo',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=255)),
                ('done', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name_plural': "ToDo's",
                'verbose_name': 'ToDo',
            },
        ),
    ]
