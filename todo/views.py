from django.views.generic import CreateView, RedirectView, UpdateView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.list import MultipleObjectMixin
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse

from .models import ToDo


class ToDoListView(MultipleObjectMixin, CreateView):
    
    model = ToDo
    template_name = 'todo/index.html'
    active_menu = 'all'
    
    success_url = reverse_lazy('home')
    fields = ('description', )
    
    def get_context_data(self, **kwargs):
        self.object_list = self.get_queryset()
        context = super(ToDoListView, self).get_context_data(**kwargs)
        context['todos_left'] = ToDo.objects.filter(done=False).count()
        context['active_menu'] = self.active_menu
        return context
    
    def form_valid(self, form):
        obj = form.save()
        if self.request.is_ajax():
            return HttpResponse(obj.pk)
        return super(ToDoListView, self).form_valid(form)


class ActiveToDoListView(ToDoListView):

    active_menu = 'active'
    
    def get_queryset(self):
        qs = super(ActiveToDoListView, self).get_queryset()
        return qs.filter(done=False)


class DoneToDoListView(ToDoListView):

    active_menu = 'done'
    
    def get_queryset(self):
        qs = super(DoneToDoListView, self).get_queryset()
        return qs.filter(done=True)

class ProcessToDoView(SingleObjectMixin, RedirectView):
    
    model = ToDo
    url = reverse_lazy('home')
    permanent = False
    
    def process(self):
        raise NotImplementedError()
    
    def get(self, request, *args, **kwargs):
        self.process()
        if request.is_ajax():
            return HttpResponse('OK')
        return super(ProcessToDoView, self).get(request, *args, **kwargs)
    

class ToggleDoneView(ProcessToDoView):
    
    def process(self):
        todo = self.get_object()
        todo.done = not todo.done
        todo.save()


class DeleteToDoView(ProcessToDoView):
    
    def process(self):
        todo = self.get_object()
        todo.delete()


class UpdateToDoView(UpdateView):

    model = ToDo
    fields = ('description', )
    
    def form_valid(self, form):
        obj = form.save()
        return HttpResponse('OK')
