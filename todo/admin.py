from django.contrib import admin

from .models import ToDo


@admin.register(ToDo)
class ToDoAdmin(admin.ModelAdmin):
    
    list_display = ('description', 'done',)
    list_editable = ('done', )