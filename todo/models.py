from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models


@python_2_unicode_compatible
class ToDo(models.Model):
    
    description = models.CharField(max_length=255)
    done = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'ToDo'
        verbose_name_plural = "ToDo's"

    def __str__(self):
        return self.description
