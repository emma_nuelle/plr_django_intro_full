# Welcome to Emma's Django ToDo

- This project is licenced under the [MIT License](http://opensource.org/licenses/MIT)
- The CSS provided as part of the template is a work by [Sindre Sorhus](http://sindresorhus.com/) licenced under the [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/deed.en_US)
- [JQuery](https://jquery.com/) is published under its own [version of the MIT License](https://github.com/jquery/jquery/blob/master/LICENSE.txt) 
## Contributing

If you'd like to contribute to this template in any way, feel free to ask questions, create issues or Pull Requests but first, make sure you read our [Code of Conduct](./COC.md).
